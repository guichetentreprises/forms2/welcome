export const environment = {
  production: true,
  nashWsUrl: 'http://localhost:10080/nash-ws-server/api',
  nashFrontUrl: 'http://localhost:8080/nash-client-forge',
  createUser: 'http://localhost:8080/account/users/new',
  login: 'http://localhost:8080/account/session/new',
  profil: 'http://localhost:8080/account/users/modify',
  logout: 'http://localhost:8080/account/disconnect'
};
