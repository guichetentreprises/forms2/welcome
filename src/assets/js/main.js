requirejs([ 'jquery', 'bootstrap', 'lib/pageLoader', 'lib/themes', 'lib/authentication', 'jquery.counterup' ], function ($, _, PageLoader) {

    $(function () {
        $('[data-toggle="popover"]').on('click', function (evt) { evt.preventDefault(); evt.stopPropagation(); }).popover();

        var path = (window.location.pathname || '');

        $('#global-navigation > ul > li.active').removeClass('active');
        $('#global-navigation a').each(function () {
            var lnk = $(this), href = lnk.attr('href');
            if (!href || path.substr(path.length - href.length) != href) {
                return;
            }

            lnk.closest('[role="presentation"]').addClass('active');
        });

        $('.number').counterUp({
            delay: 25,
            time: 1000
        });

        $('[data-background]').each(function () {
            var elm = $(this), url = elm.data('background');
            elm.css('background-image', 'url(' + url + ')');
        });
    });

    console.debug('page loaded');

});
