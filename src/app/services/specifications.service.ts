import { Injectable } from '@angular/core';
import { Http, Response, Headers, URLSearchParams, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

import { ListResult } from './api/list-results.interface';
import {environment} from '../../environments/environment';
@Injectable()
export class SpecificationsService {

  constructor(protected http: Http) { }


  list(search: string = null, page: number = 1, limit: number = 10,filtre:string=null): Observable<ListResult<Specification>> {
    let myParams = new URLSearchParams();
    var startIndex = (page - 1) * limit;
    var maxResult = limit;
    if (search) myParams.set('q', search)
    if (page) myParams.set('startIndex', String(startIndex))
    if (limit) myParams.set('maxResults', String(maxResult))
    if(filtre) myParams.set('filters',"groupe:"+filtre)

    let myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: myHeaders, params: myParams });

    return this.http.get(environment.nashWsUrl + '/v1/Specification/search', options).map(res => res.json())
  }

  findAllGroup():any{
    return this.http.get(environment.nashWsUrl + '/v1/Specification/group').map(res => res.json().groups);
  }

  transform(collection: any, property: string): any {
    // prevents the application from breaking if the array of objects doesn't exist yet
    if (!collection) {
      return null;
    }

    const groupedCollection = collection.reduce((previous, current) => {
      if (!previous[current[property]]) {
        previous[current[property]] = [current];
      } else {
        previous[current[property]].push(current);
      }

      return previous;
    }, {});

    // this will return an array of objects, each object containing a group of objects
    return Object.keys(groupedCollection).map(key => ({ key, value: groupedCollection[key] }));
  }

}


export class Specification {

  constructor(public author: string,
    public title: string) {
  }
}
