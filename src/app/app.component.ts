import { Component } from '@angular/core';
import {environment} from '../environments/environment';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'app';
  createUser = environment.createUser + '?callback=' + window.location;
  login = environment.login + '?callback=' + window.location;
  logout = environment.logout + '?callback=' + window.location;
  profil = environment.profil + '?callback=' + window.location;
  home = environment.home;
  myRecords = environment.myRecords;
}
