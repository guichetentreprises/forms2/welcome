import { Component, OnInit } from '@angular/core';
import { Specification, SpecificationsService } from '../../services/specifications.service';
import 'rxjs/add/operator/map';
import 'rxjs/Subscription';
import 'rxjs/add/operator/merge';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/pluck';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { groupBy, mergeMap, toArray } from 'rxjs/operators';
import { GroupByPipe } from '../../services/api/cutom.pipe';
import {environment} from '../../../environments/environment';
@Component({
  selector: 'app-specifications',
  templateUrl: './specifications.component.html',
  styleUrls: ['./specifications.component.less']
})
export class SpecificationsComponent implements OnInit {

  total$: Observable<number>;
  groups$:Array<String>;
  items$: Observable<Specification[]>;

  public specifications:Specification[];
  public specificationsFiltred:String[];
  searchTerm$ = new Subject<string>();
  terms: string = '';
  page: number = 1;
  frontUrl : string = environment.nashFrontUrl;
  maxPerPage: number = 10;
  public grouby : GroupByPipe;
  private pageStream = new Subject<number>();


  constructor(private data:SpecificationsService) {
   }


  ngOnInit() {
    const component = this;
      const searchSource = this.searchTerm$
      .debounceTime(1000)
      .distinctUntilChanged()
      .map(searchTerm => {
        this.terms = searchTerm
        return {search: searchTerm, page:1,limit:this.maxPerPage}
      });

      const pageSource = this.pageStream.map(pageNumber => {
        this.page = pageNumber;
        return {search: this.terms, page: pageNumber, limit:this.maxPerPage}
      });

      const source = pageSource
      .merge(searchSource)
      .startWith({search: this.terms, page: this.page,limit:this.maxPerPage})
      .switchMap((params: {search: string, page: number, limit:number}) => {
        return this.data.list(params.search, params.page,params.limit)
      })
      .share();
      this.total$ = source.pluck('totalResults');
      this.items$ = source.pluck('content');
      this.data.findAllGroup().subscribe( groups => this.groups$ = groups);
      this.items$.subscribe(spec => this.specifications = spec);
     // this.groups$.subscribe(spec => this.specificationsFiltred = spec);

  }

  clickedLink(title : string){

    //console.log(this.data.transform(this.specifications,"title"));
    //const spec = this.data.transform(this.specifications,"title").map(spec => spec.key == title ? this.specificationsFiltred =spec.value:false);
/*     console.log("toto");
    const pageSource = this.pageStream.map(pageNumber => {
      this.page = pageNumber;
      return {search: this.terms, page: pageNumber, limit:this.maxPerPage}
    });

    const source = pageSource
    .startWith({search: this.terms, page: this.page,limit:this.maxPerPage})
    .switchMap((params: {search: string, page: number, limit:number}) => {
      console.log(params.limit);
      return this.data.list(params.search, params.page,params.limit,title)
    })
    .share();
    console.log(source);
    this.total$ = source.pluck('totalResults');
    this.items$ = source.pluck('content');
    this.items$.subscribe(spec => this.specifications = spec); */
    const source =this.data.list("",0,10,title);
    this.items$ = source.pluck('content');
    this.total$ = source.pluck('totalResults')
    this.items$.subscribe(spec => this.specifications = spec);
  }
  goToPage(page: number) {
    this.pageStream.next(page)
  }

}
