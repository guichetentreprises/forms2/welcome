import * as _ from 'lodash'
import { Component, Input, EventEmitter, Output } from '@angular/core'
import { Location } from '@angular/common'

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.less']
})
export class PaginationComponent {
  totalPage: number = 0

  @Input()
  params: { [key: string]: string | number } = {}

  @Input()
  total: number = 0

  @Input()
  maxPerPage: number = 10

  @Input()
  page: number = 1

  @Output()
  goTo: EventEmitter<number> = new EventEmitter<number>()

  constructor() { }

  totalPages() {
    return Math.ceil(this.total / this.maxPerPage)
  }

  rangeStart() {
    return 1
  }

  pagesRange() {
    return _.range(this.rangeStart(), this.totalPages() + 1);
  }

  prevPage() {
    return Math.max(this.rangeStart(), this.page - 1)
  }

  nextPage() {
    return Math.min(this.page + 1, this.totalPages())
  }

  pageParams(page: number) {
    let params = _.clone(this.params)
    params['page'] = page
    return params
  }
  showPage(page: number) {
    if(this.totalPages() > 8){
      if( (page - this.page < 4 && page - this.page >= 0 && this.page<4 ) || page - this.page < 3 && this.page <= page||page == 1 || (this.totalPages() - this.page < 4 && this.totalPages() - page < 4 && this.page >4) ||this.totalPages() - page == 0 ){
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

  pageClicked(page: number) {
    if (page <= this.totalPages()) {
      this.goTo.next(page);
    }
  }
}
