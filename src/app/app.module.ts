import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpModule} from '@angular/http';


import { AppComponent } from './app.component';
import { SpecificationsComponent } from './components/specifications/specifications.component';
import { SpecificationsService } from './services/specifications.service';
import { PaginationComponent } from './components/pagination/pagination.component';
import { GroupByPipe } from './services/api/cutom.pipe';


@NgModule({
  declarations: [
    AppComponent,
    SpecificationsComponent,
    PaginationComponent,
    GroupByPipe
  ],
  imports: [
    BrowserModule,
    HttpModule
  ],
  providers: [SpecificationsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
