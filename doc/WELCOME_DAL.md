@page DAL_Welcome
Welcome: Dossier d'Architecture Logiciel
============================================

\tableofcontents

\section desc Description des exigences

\subsection exigMet Exigences métiers

Le projet « welcome» permet d'afficher les formalités publié on se basant sur une api Rest public de nash.

\subsection cntrPerf Contrainte de performance
> Sans objet

\subsection cntrDispo Contrainte de de disponibilité

Interruptions de services prévisibles : installation, mise à jour applicative.

Aucune contrainte de disponibilité n'est définie.

\subsection cntrVolum Contrainte de volumétrie


\subsection archiLogique Schéma de l'architecture logique
![Schéma de l'architecture logique](@ref schema_architecture_logique_payment.jpg)

\section archi Architecture logicielle

\subsection decompo Décomposition en sous-systèmes applicatifs

>   Description des nœuds applicatifs et des services que chacun rend.

| **type de noeud applicatif** | **nom de noeud applicatif** | **description**                                                              	|
|------------------------------|-----------------------------|----------------------------------------------------------------------------------|
| apache_vhost                 | WELCOME_VH                | virtual host Apache qui sert index.html                                 		|
                                            		|

>   Explication des types de noeuds applicatifs : 

| **type de noeud applicatif** | **description**  |
|------------------------------|------------------|
| apache_vhost  | un virtual host qui contient les fichiers html js et css

\subsection descMacro Description macroscopique des flux internes

>   Liste des flux internes au SI ou au VLAN, à choisir

| **Origine**   | **Destination** 	| **Protocol** | **Port** |
|---------------|-------------------|--------------|----------|
| PAYMENT_VH	  | NASH_WS_APP    	  | HTTPS        | 443     |


\subsection cineAlim Cinématique d'alimentation

>   Sans objet.

\subsection descMacroFlux Description macroscopique des flux externes

| **Origine**   | **Destination** 	| **Protocol** 	| **Port** 	|
|---------------|-------------------|---------------|-----------|
| Internet		  | WELCOME_VH		    | HTTPS         | 443     	|
| Internet	    | NASH_WS_VH   		  | HTTPS         | 443     	|



\subsection exploit Eléments d'exploitation

Aucune remarque.
