@page DI_Welcome
Welcome : Dossier d'installation
===================================

\tableofcontents

\section compoBulle Composants de la bulle welcome

\subsection welcome

IHM public

-   nom de projet : nash-welcome

-   URL : https://welcome.${env}.guichet-entreprises.fr

-   bulle : welcome

| **GroupID**          	| **Nom du fichier**                 | **Description** |
|-----------------------|------------------------------------|-----------------|
| fr.ge.common.welcome 	| nash-welcome-*x.y.z.tar.gz   | l'ensemble des fichiers statiques de l'application

* *x.y.z* représente la version livrée.

\subsection confSocle Configuration du socle logiciel

Doivent être provisionnés sur les serveurs les composants suivant :

| **Noeud applicatif** 	| **Description**              | **Composants techniques requis** |
|-----------------------|------------------------------|----------------------------------|
| WELCOME_VH        	| Frontal du module applicatif | Apache                           |            |


\subsection instVirtualHost Installation du virtual host

| **Nom**  	| **Description**           | **&lt; PROD**                         | **PROD**                    		|
|-----------|---------------------------|---------------------------------------|----------------
| Welcome 	| URL de l'instance interne | welcome.${env}.guichet-entreprises.fr |welcome.guichet-entreprises.fr 	|


Pour l'exemple, dans le répertoire **&nbsp;/etc/apache2/sites-enabled**, ajouter un virtualhost (ie **XX-welcome.${env}.guichet-entreprises.fr.conf**)


```xml
	#********************************************
	# Vhost template in module puppetlabs-apache
	# Managed by Puppet
	#********************************************
	
	<VirtualHost *:8080>
		ServerName welcome.${env}.guichet-entreprises.fr
		
		## Vhost docroot
		DocumentRoot "/var/www/ge"
		
		
		<Directory "/var/www/ge">
			Options None
			AllowOverride None
			Require all denied
		</Directory>
		
		## Logging
		ErrorLog "/var/log/apache2/welcome.${env}.guichet-entreprises.fr_error.log"
		ServerSignature Off
		CustomLog "/var/log/apache2/welcome.${env}.guichet-entreprises.fr_access.log"
		combined
		
	</VirtualHost>
```

\subsubsection confPropertiesApp fichier application.properties

```
# build angular
"production": true,
#URL public of  nash-ws to display all published specification 
"nashWsUrl": "http://localhost:10080/nash-ws-server/api",
#URL to nash-web to create record 
"nashFrontUrl": "http://localhost:8080/nash-client-web",
#URL account to create user
"createUser": "http://localhost:10080/ge-account-mock/users/new",
#URL account to authenticate
"login": "http://localhost:10080/ge-account-mock/session/new",
#URL account to modify user profil
"profil": "http://localhost:10080/ge-account-mock/users/modify",
#URL account to logout
"logout": "http://localhost:10080/ge-account-mock/disconnect"
#WWW public URL
"home": "http://localhost:8080/nash-client-web"
#DASHBOARD GE/GQ public URL
"myRecords": "http://localhost:10080/ge-dashboard-webclient"
```
